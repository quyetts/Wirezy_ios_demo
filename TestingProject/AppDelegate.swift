//
//  AppDelegate.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/16/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainNavigation: UINavigationController!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        initializeSomeStuff()
        setupRootView()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    /**
     *  initialize some stuff
     */
    func initializeSomeStuff() {
        // set up uinavigationbar
        UINavigationBar.appearance().barTintColor = Constants.kHeaderColor
        UINavigationBar.appearance().tintColor = Constants.kHeaderTitleColor
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : Constants.kHeaderTitleColor]
    }
    
    func setupRootView() {
        let mainVC = CategoryViewController(nibName: "CategoryViewController", bundle: nil)
        let navVC = BaseNavigationController.sharedInstance
        navVC.viewControllers = [mainVC]
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navVC
        self.window?.makeKeyAndVisible()
        PXRequest.setConsumerKey("VmVsPz7fN2ghxfrEMJQUXHj5ExCLiP4NEzadc4Qj", consumerSecret: "1kBnX2EMq2vWeY9HX1c7qecCFbchLpkg61fbqnAS")
    }


}

