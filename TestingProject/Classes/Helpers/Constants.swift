//
//  Constants.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/16/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit
import SwiftyJSON

class Constants: NSObject {
    
// MARK: - CompletionHandler
    typealias CompletionHandler = (Bool, AnyObject?) -> ()
    
// MARK:- Color
    
    static let kHeaderColor = UIColor.colorFromHexString(hex: "#00A6AD")
    static let kHeaderTitleColor = UIColor.white

// MARK:- IPhone, IOS Version and Size

    static let IS_IPAD = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
    static let IS_IPHONE = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
    static let IS_RETINA = (UIScreen.main.scale >= 2.0)
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = fmax(SCREEN_HEIGHT, SCREEN_WIDTH)
    static let SCREEN_MIN_LENGTH = fmin(SCREEN_WIDTH, SCREEN_HEIGHT)
    
    static let IS_IPHONE_4_OR_LESS = (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
    static let IS_IPHONE_5 = (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
    static let IS_IPHONE_6 = (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
    static let IS_IPHONE_6P = (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
    
    static let WINSIZE = UIApplication.shared.keyWindow?.frame.size
    static let SCREENSIZE = UIScreen.main.bounds.size
    
// MARK:- Standard UserDefault
    static let userDefaults = UserDefaults.standard
}
