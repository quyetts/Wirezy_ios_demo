//
//  PhotoModel.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/20/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit
import SwiftyJSON

class PhotoModel: NSObject {
    
    // Varialbes
    var authorName: String = ""
    var photoDescription: String = ""
    var authorAvatarUrl: String = ""
    var photoUrl: String = ""
    
    // Init
    required init(json: JSON) {
        if (json.null == nil) {
            authorName = json["user"]["fullname"].null == nil ? json["user"]["fullname"].string! : ""
            photoDescription = json["description"].null == nil ? json["description"].string! : ""
            photoUrl = json["image_url"].arrayValue[0].null == nil ? json["image_url"].arrayValue[0].string! : ""
            authorAvatarUrl = json["user"]["avatars"]["default"]["https"].null == nil ? json["user"]["avatars"]["default"]["https"].string! : ""
        }
    }
}
