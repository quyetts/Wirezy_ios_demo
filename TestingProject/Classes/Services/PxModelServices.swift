//
//  PxModelServices.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/20/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit
import SwiftyJSON

class PxModelServices: NSObject {
    
    //fetch data of model follow page
    class func fetchDataOfModel(model: PXPhotoModelCategory, resultsPerPage: Int, page: Int, completionHandler: @escaping Constants.CompletionHandler) {
        
        _ = PXRequest.init(for: .popular, resultsPerPage: resultsPerPage, page: page, photoSizes: .large, sortOrder: .createdAt, except: .PXAPIHelperUnspecifiedCategory, only: model) { (data, error) in
            if error == nil {
                let json: JSON = JSON(data!)
                
                let arrayPhotos: Array<JSON> = json["photos"].arrayValue
                var arrayPhotoModel: Array = Array<PhotoModel>()
                
                arrayPhotos.forEach({ (aJson) in
                    let photoModel = PhotoModel(json: aJson)
                    arrayPhotoModel.append(photoModel)
                })
                
                completionHandler(true, arrayPhotoModel as AnyObject?)
            } else {
                completionHandler(false, data as AnyObject?)

            }
        }
        
        
        
    }
}
