//
//  BaseNavigationController.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/16/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    // Instance
    internal static let sharedInstance = BaseNavigationController()
    
//MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.extendedLayoutIncludesOpaqueBars = false
        self.navigationBar.isTranslucent =  false
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -80), for: .default)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}
