//
//  BaseViewController.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/16/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
