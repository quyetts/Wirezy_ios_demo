//
//  CategoryViewController.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/16/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit

class CategoryViewController: BaseViewController {
    
    // Oulet and variables
    @IBOutlet weak var tableCategory: UITableView!
    var ArrayCategories: NSArray!
    var arrayPXModel: NSArray!
    let cellIdentifier = "categoryCell"

// MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Categories"
        initData()
        initComponents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
// MARK: - Init

    func initData() {
        self.ArrayCategories = NSArray.init(array: ["Uncategorized","Abstract", "Animals", "Black and White", "Celebrities", "City and Architecture", "Commericial", "Concert", "Family", "Fashion", "Film", "Fine Art", "Food", "Journalism", "Landscapes", "Macro", "Nature", "Nude", "People", "Performing Arts", "Sport", "Still Life", "Street", "Transportation", "Travel", "Underwater", "Urban Exploration", "Wedding"])
        self.arrayPXModel = NSArray.init(array: [PXPhotoModelCategory.PXPhotoModelCategoryUncategorized,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryAbstract,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryAnimals,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryBlackAndWhite,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryCelbrities,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryCityAndArchitecture,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryCommercial,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryConcert,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryFamily,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryFashion,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryFilm,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryFineArt,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryFood,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryJournalism,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryLandscapes,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryMacro,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryNature,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryNude,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryPeople,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryPerformingArts,
                                                 PXPhotoModelCategory.PXPhotoModelCategorySport,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryStillLife,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryStreet,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryTransportation,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryTravel,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryUnderwater,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryUrbanExploration,
                                                 PXPhotoModelCategory.PXPhotoModelCategoryWedding])
        
    }
    
    func initComponents() {
        self.tableCategory.register(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
    }
    
}

extension CategoryViewController: UITableViewDataSource, UITableViewDelegate {
    
//MARK: - TableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ArrayCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
        cell.textLabel?.text = self.ArrayCategories[indexPath.row] as? String
        return cell
    }
    
//MARK: - TableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let recentPhotosVC = RecentPhotosViewController(nibName: "RecentPhotosViewController", bundle: nil)
        recentPhotosVC.naviTitle = self.ArrayCategories[indexPath.row] as? String
        recentPhotosVC.model = self.arrayPXModel[indexPath.row] as! PXPhotoModelCategory
        self.navigationController?.pushViewController(recentPhotosVC, animated: true)
    }
    
    
}
