//
//  RecentPhotosCollectionViewCell.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/20/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit

class RecentPhotosCollectionViewCell: UICollectionViewCell {
    
    // Outlet
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageAuthorAvatar: UIImageView!
    @IBOutlet weak var labelAuthorName: UILabel!
    @IBOutlet weak var imageRecentPhoto: UIImageView!
    
//MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageAuthorAvatar.layer.cornerRadius = self.imageAuthorAvatar.bounds.size.width / 2
        self.imageAuthorAvatar.clipsToBounds = true
    }

}
