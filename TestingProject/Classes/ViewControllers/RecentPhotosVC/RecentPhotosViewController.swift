//
//  RecentPhotosViewController.swift
//  TestingProject
//
//  Created by Hoang Duoc on 3/20/17.
//  Copyright © 2017 Hoang Duoc. All rights reserved.
//

import UIKit

class RecentPhotosViewController: UIViewController {
    
    // Outlet and variables
    @IBOutlet weak var collectionRecentPhotos: UICollectionView!
    let cellIdentifier = "photoCell"
    var naviTitle: String!
    let itemRatio: CGFloat = 15/18
    
    var model: PXPhotoModelCategory!
    var arrayPhotos: Array = Array<PhotoModel>()
    var arrayBrowserPhotos: Array = Array<MWPhoto>()

    var currentPage: Int = 1
    var hasLoadMore: Bool = true
    var hasPullToRefresh: Bool = true
    let resultsPerPage: Int = 20

    
// MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
        createAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
// MARK: - Init
    
    func initComponents() {
        self.collectionRecentPhotos.register(UINib.init(nibName: "RecentPhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: self.cellIdentifier)
        self.title = self.naviTitle
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
// MARK: - Private method
    
    func createAction() {
        
        // First load
        PxModelServices.fetchDataOfModel(model: self.model, resultsPerPage: self.resultsPerPage, page: self.currentPage) { (status, data) in
            self.hasPullToRefresh = true
            if status {
                let arrayResults: Array = data as! Array<PhotoModel>
                if arrayResults.count < self.resultsPerPage {
                    self.hasLoadMore = false
                }
                self.handleResult(data: data!)
            }
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        
        // Add load more
        self.collectionRecentPhotos.addInfiniteScrolling { 
            self.loadMore()
        }
        
        // Add pull to refresh
        self.collectionRecentPhotos.addPullToRefresh { 
            self.pulltoRefresh()
        }
    }
    
    // Handle results of PXRequest
    func handleResult(data: AnyObject) {
        let photos: Array = data as! Array<PhotoModel>
        photos.forEach({ (photoModel) in
            self.arrayPhotos.append(photoModel)
        })
        self.collectionRecentPhotos.reloadData()
    }
    
    
    func pulltoRefresh() {
        if self.hasPullToRefresh {
            self.hasPullToRefresh = false
            self.currentPage = 1
                PxModelServices.fetchDataOfModel(model: self.model, resultsPerPage: self.resultsPerPage, page: self.currentPage) { (status, data) in
                    self.collectionRecentPhotos.pullToRefreshView.stopAnimating()
                    if status {
                        self.arrayPhotos.removeAll()
                        self.handleResult(data: data!)
                    }
                    self.hasPullToRefresh = true
                }
        }
    }
    
    func loadMore() {
        if hasLoadMore {
            hasLoadMore = false
            self.currentPage += 1
                PxModelServices.fetchDataOfModel(model: self.model, resultsPerPage: self.resultsPerPage, page: self.currentPage) { (status, data) in
                    self.collectionRecentPhotos.infiniteScrollingView.stopAnimating()
                    if status {
                        let arrayResults: Array = data as! Array<PhotoModel>
                        if arrayResults.count >= self.resultsPerPage {
                            self.hasLoadMore = true
                        }
                        self.handleResult(data: data!)
                    }
                }
        }
    }
}

extension RecentPhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MWPhotoBrowserDelegate {
    
//MARK: - CollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! RecentPhotosCollectionViewCell
        photoCell.labelAuthorName.text = self.arrayPhotos[indexPath.row].authorName
        photoCell.labelDescription.text = self.arrayPhotos[indexPath.row].photoDescription
        photoCell.imageRecentPhoto.sd_setImage(with: URL(string: self.arrayPhotos[indexPath.row].photoUrl), placeholderImage: UIImage(named: "user_test"))
        photoCell.imageAuthorAvatar.sd_setImage(with: URL(string: self.arrayPhotos[indexPath.row].authorAvatarUrl), placeholderImage: UIImage(named: "user_test"))
        return photoCell
    }
//MARK: - CollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        // create data for browser
        self.arrayBrowserPhotos.removeAll()
        self.arrayPhotos.forEach { (photoModel) in
            if let url = URL(string: photoModel.photoUrl) {
                let photo: MWPhoto = MWPhoto(url: url)
                photo.caption = photoModel.photoDescription
                self.arrayBrowserPhotos.append(photo)
            }
        }
        
        // init browser
        let browser = MWPhotoBrowser(delegate: self)
        browser?.displayActionButton = true;
        browser?.displayNavArrows = true;
        browser?.displaySelectionButtons = false;
        browser?.alwaysShowControls = true;
        browser?.zoomPhotosToFill = false;
        browser?.enableGrid = true;
        browser?.startOnGrid = true;
        browser?.enableSwipeToDismiss = false;
        browser?.autoPlayOnAppear = true;
        browser?.setCurrentPhotoIndex(UInt(indexPath.row))
        self.navigationController?.pushViewController(browser!, animated: true)
        
    }
    
//MARK: - CollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return calSizeForCell(collectionView: collectionView)
    }
    
    // Calculate item size of collectionView
    func calSizeForCell(collectionView: UICollectionView) -> CGSize{
        
        let collectionViewLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        
        let itemSpacing = collectionViewLayout!.minimumInteritemSpacing
        let leftInset = collectionViewLayout!.sectionInset.left
        let rightInset = collectionViewLayout!.sectionInset.left
        
        let itemWidth = (Constants.SCREEN_WIDTH - leftInset - rightInset - itemSpacing) / 2
        let itemHeight = itemWidth / self.itemRatio
        
        let itemSize: CGSize = CGSize(width: itemWidth, height: itemHeight)
        
        return itemSize
    }

//MARK: - MWPhotoBrowserDelegate
    
    func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
        return UInt(self.arrayBrowserPhotos.count)
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
        if index < UInt(self.arrayBrowserPhotos.count) {
            return self.arrayBrowserPhotos[Int(index)]
        }
        return nil
    }
}

